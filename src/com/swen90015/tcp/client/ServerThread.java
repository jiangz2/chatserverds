package com.swen90015.tcp.client;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ServerThread implements Runnable {
	private String identity = "";
	private String room = "";
	private Socket socket = null;
	private String createRoom = "";
	boolean disconnect = false;
	private Integer port;
	private String remoteServer;
	
	public ServerThread(String remoteServer, Integer port){
		this.remoteServer = remoteServer;
		this.port = port;
	}

	public void run() {
		try {
			// connect to a server listening
			socket = new Socket(remoteServer, port);

			// Receiving command line input from client using a new thread
			Thread commandLine = new Thread(new CLThread(this));
			commandLine.start();

			// Preparing receiving streams from server
			DataInputStream in = new DataInputStream(socket.getInputStream());
			DecodingClient decodingClient = new DecodingClient();

			while (true) {
				String msg = in.readUTF();
				JSONParser parser = new JSONParser();
				JSONObject fromServer = (JSONObject) parser.parse(msg);
				String type = decodingClient.decoding(fromServer);
				switch (type) {
				case "newidentity":
					identityChange(decodingClient.getNewIdentity(fromServer));
					printPrefix();
					break;
				case "roomchange":
					// disconnect room change
					if (fromServer.get("roomid").equals("")) {
						roomChange(decodingClient.getRoomChange(fromServer));
						disconnect = true;
						break;
					}
					// normal room change
					else {
						roomChange(decodingClient.getRoomChange(fromServer));
						printPrefix();
						break;
					}
				case "roomcontent":
					decodingClient.getRoomContent(fromServer);
					printPrefix();
					break;

				case "roomlist":
					decodingClient.getRoomList(fromServer, createRoom);
					// reset createRoom to null
					createRoom = "";
					printPrefix();
					break;
				case "message":
					System.out.println(fromServer.get("identity") + ": "
							+ fromServer.get("content"));
					break;
				}

				if (disconnect == true) {
					// server.stop();
					break;
				}
			}

			in.close();
			socket.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void identityChange(JSONObject object) {
		if (object.get("former").equals(identity)) {
			setIdentity((String) object.get("identity"));
		}
	}

	public void roomChange(JSONObject object) {
		if (object.get("identity").equals(identity)) {
			setRoom((String) object.get("roomid"));
		}

	}

	public void printPrefix() {
		System.out.println("[" + room + "] " + identity + ">");
	}

	public Socket getSocket() {
		return socket;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getCreateRoom() {
		return createRoom;
	}

	public void setCreateRoom(String createRoom) {
		this.createRoom = createRoom;
	}

}
