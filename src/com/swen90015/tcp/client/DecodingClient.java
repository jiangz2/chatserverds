package com.swen90015.tcp.client;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class DecodingClient {

	// decode message from server to get message type
	public String decoding(JSONObject object) throws ParseException {
		String type = (String) object.get("type");
		return type;
	}

	// decode message from server to get new identity
	public JSONObject getNewIdentity(JSONObject object) throws IOException,
			ParseException {
		String identity = (String) object.get("identity");
		if (object.get("former").equals("")) {
			System.out.println("Client Connected as " + identity);
		} else if (object.get("former").equals(object.get("identity"))) {
			System.out.println("Requested identity invalid or in use.");
		} else {
			System.out.println(object.get("former") + " is now " + identity);
		}
		return object;
	}

	// decode message from server to get change room message
	public JSONObject getRoomChange(JSONObject object) {
		String roomMessage = "";
		// initial room change
		if (object.get("former").equals("")) {
			roomMessage = object.get("identity") + " moved to MainHall";
		}
		// client room change rejected
		else if (object.get("former").equals(object.get("roomid"))) {
			roomMessage = "The requested room is invalid or non existent.";
		}
		// client room change successfully
		else {
			roomMessage = object.get("identity") + " moved from "
					+ object.get("former") + " to " + object.get("roomid");
		}
		System.out.println(roomMessage);
		return object;
	}

	public void getRoomContent(JSONObject object) {
		String roomContent = "";
		String identities = "";
		@SuppressWarnings("unchecked")
		List<String> ids = (List<String>) object.get("identities");
		if (!ids.isEmpty()) {
			Iterator<String> it = ids.iterator();
			while (it.hasNext()) {
				identities = identities + it.next() + " ";
			}
		}
		roomContent = object.get("roomid") + " contains " + identities;
		System.out.println(roomContent);
	}

	@SuppressWarnings("unused")
	public void getRoomList(JSONObject object, String createRoom) {
		JSONArray roomsArray = (JSONArray) object.get("rooms");
		// if it is a create room response
		if (!createRoom.equals("")) {
			if (roomsArray == null) {
				System.out.println("Room " + createRoom
						+ " is invalid or already in use.");
			} else {
				System.out.println("Room " + createRoom + " created.");
				Iterator rooms = roomsArray.iterator();
				while (rooms.hasNext()) {
					JSONObject room = (JSONObject) rooms.next();
					System.out.println(room.get("roomid") + ": "
							+ room.get("count") + " guest");
				}
			}
		}
		// it is a normal list response
		else {
			Iterator rooms = roomsArray.iterator();
			while (rooms.hasNext()) {
				JSONObject room = (JSONObject) rooms.next();
				System.out.println(room.get("roomid") + ": "
						+ room.get("count") + " guest");
			}
		}
	}

	// to verify if the room is created successfully
	public boolean createVerify(JSONObject object, String createRoom) {
		boolean isVerify = false;
		JSONArray roomsArray = (JSONArray) object.get("rooms");
		Iterator rooms = roomsArray.iterator();
		while (rooms.hasNext()) {
			JSONObject room = (JSONObject) rooms.next();
			if (room.get("roomid").equals(createRoom)) {
				isVerify = true;
			}
		}
		return isVerify;
	}

}
