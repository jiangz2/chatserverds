package com.swen90015.tcp.client;

import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;

public class Room {
	private String roomIdentity = "";
	private String owner = "";
	private ConcurrentHashMap<String, Socket> clients = new ConcurrentHashMap<String, Socket>();
	
	public String getRoomIdentity() {
		return roomIdentity;
	}
	public void setRoomIdentity(String roomIdentity) {
		this.roomIdentity = roomIdentity;
	}
	public ConcurrentHashMap<String, Socket> getClients() {
		return clients;
	}
	public void setClients(ConcurrentHashMap<String, Socket> clients) {
		this.clients = clients;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}


}
