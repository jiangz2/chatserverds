package com.swen90015.tcp.server;

import java.io.IOException;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

public class ServerEntry {

	public static void main(String[] args) throws IOException {
		// parse cmdLine input
		Arg arg = new Arg();
		CmdLineParser parser = new CmdLineParser(arg);
		try {
			parser.parseArgument(args);
		} catch (CmdLineException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// new a thread to listen to client
		Thread server = new Thread(new Server(arg.port));
		server.start();

	}

}
