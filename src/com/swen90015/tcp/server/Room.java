package com.swen90015.tcp.server;

import java.util.concurrent.ConcurrentHashMap;

public class Room {
	private String roomIdentity = "";
	private String owner = "";
	private ConcurrentHashMap<String, ClientThread> clients = new ConcurrentHashMap<String, ClientThread>();
	
	public String getRoomIdentity() {
		return roomIdentity;
	}
	public void setRoomIdentity(String roomIdentity) {
		this.roomIdentity = roomIdentity;
	}
	
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public ConcurrentHashMap<String, ClientThread> getClients() {
		return clients;
	}
	public void setClients(ConcurrentHashMap<String, ClientThread> clients) {
		this.clients = clients;
	}




}
