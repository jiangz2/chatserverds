package com.swen90015.tcp.server;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import org.json.simple.JSONObject;

public class Server implements Runnable {
	// to keep all rooms clients information in this hash map
	private ConcurrentHashMap<String, Room> roomMap = new ConcurrentHashMap<String, Room>();
	private ServerSocket serverSocket = null;
	//to generate the identity for user
	private int id = 0;
	// tcp port
	private Integer port;
	// to store kick history
	private ConcurrentHashMap<String, JSONObject> kick = new ConcurrentHashMap<String, JSONObject>();

	// initialize the roomMap
	public Server(Integer port) {
		this.port = port;
		Room MainHall = new Room();
		MainHall.setRoomIdentity("MainHall");
		MainHall.setOwner(null);
		getRoomMap().put(MainHall.getRoomIdentity(), MainHall);
	}

	public void run() {
		// Server is listening
		try {
			serverSocket = new ServerSocket(port);
			System.out.println("Server is listening...");

			while (true) {
				// Server waits for a new connection
				Socket socket = serverSocket.accept();
				// Java creates new socket object for each connection.

				System.out.println("Client Connected...");

				// A new thread is created per client
				Thread client = new Thread(new ClientThread(socket, this));
				// It starts running the thread by calling run() method
				client.start();

			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String messageCentral(JSONObject object) {
		String type = (String) object.get("type");
		return type;
	}

	// to send message to all clients
	public void sendMessageToAll(JSONObject object) {
		Iterator<Room> rooms = getRoomMap().values().iterator();
		while (rooms.hasNext()) {
			Room room = rooms.next();
			if(room.getClients()!=null){
				Iterator<ClientThread> clients = room.getClients().values().iterator();
				while (clients.hasNext()) {
					try {
						DataOutputStream out = new DataOutputStream(clients.next().getSocket().getOutputStream());
						out.writeUTF(object.toJSONString());
						out.flush();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		}

	}

	public String identityGenerate() {
		String identity = "guest" + id;
		id = id + 1;
		return identity;
	}

	public ConcurrentHashMap<String, Room> getRoomMap() {
		return roomMap;
	}

	public void setRoomMap(ConcurrentHashMap<String, Room> roomMap) {
		this.roomMap = roomMap;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ConcurrentHashMap<String, JSONObject> getKick() {
		return kick;
	}

	public void setKick(ConcurrentHashMap<String, JSONObject> kick) {
		this.kick = kick;
	}

}
